<?php

namespace Iweigel\NotifierBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

class IweigelNotifierExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        $this->registerRules($container, $config);
        $this->registerRecipientChannelResolver($container, $config);
    }

    /**
     * @param ContainerBuilder $container
     * @param array            $config
     */
    private function registerRules(ContainerBuilder $container, array $config)
    {
        if (!$container->has('iweigel_notifier.channel_resolver')) {
            return ;
        }
        foreach ($config['types'] as $type => $data) {
            foreach ($data['channels'] as $channelId) {
                $container->getDefinition('iweigel_notifier.channel_resolver')
                    ->addMethodCall('addRule', [$type, new Reference($channelId)]);
            }
        }
    }
    private function registerRecipientChannelResolver(ContainerBuilder $container, array $config)
    {
        if (!$container->has('iweigel_notifier.channel_resolver')) {
            return ;
        }
        $container->getDefinition('iweigel_notifier.channel_resolver')
            ->setArguments([new Reference($config['recipient_channel_resolver'])]);
    }

}
