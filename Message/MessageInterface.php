<?php

namespace Iweigel\NotifierBundle\Message;

use Iweigel\NotifierBundle\ParameterBag\ParameterBagInterface;
use Iweigel\NotifierBundle\Type\TypeInterface;

interface MessageInterface
{
    /**
     * Set the message type.
     *
     * @param TypeInterface $type
     */
    public function setType(TypeInterface $type);

    /**
     * Get the message type.
     *
     * @return TypeInterface
     */
    public function getType();

    /**
     * Get all the attached ParameterBags.
     *
     * @return ParameterBagInterface[]
     */
    public function getParameterBags();

    /**
     * Get a specific ParameterBag based on identifier.
     *
     * @param string $identifier
     * @return ParameterBagInterface
     */
    public function getParameterBag($identifier);

    /**
     * Check if this message has a specific ParameterBag.
     *
     * @param string $identifier
     * @return bool
     */
    public function hasParameterBag($identifier);

    /**
     * Add a ParameterBag.
     *
     * @param ParameterBagInterface $bag
     */
    public function addParameterBag(ParameterBagInterface $bag);
}