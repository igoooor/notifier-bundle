<?php

namespace Iweigel\NotifierBundle\Message;

use Iweigel\NotifierBundle\ParameterBag\ParameterBagTrait;
use Iweigel\NotifierBundle\Type\TypeInterface;

class Message implements MessageInterface
{
    use ParameterBagTrait;

    /**
     * @var TypeInterface
     */
    private $type;

    /**
     * @param TypeInterface $type
     */
    public function __construct(TypeInterface $type)
    {
        $this->setType($type);
    }

    /**
     * Set the message type.
     *
     * @param TypeInterface $type
     */
    public function setType(TypeInterface $type)
    {
        $this->type = $type;
    }

    /**
     * Get the message type.
     *
     * @return TypeInterface
     */
    public function getType()
    {
        return $this->type;
    }

}