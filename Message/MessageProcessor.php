<?php

namespace Iweigel\NotifierBundle\Message;

use Iweigel\NotifierBundle\Channel\ChannelInterface;
use Iweigel\NotifierBundle\Processor\ProcessorStore;
use Iweigel\NotifierBundle\Recipient\RecipientInterface;

class MessageProcessor
{
    /**
     * @var ProcessorStore
     */
    private $processorStore;

    /**
     * @param ProcessorStore $processorStore
     */
    public function __construct(ProcessorStore $processorStore)
    {
        $this->processorStore = $processorStore;
    }

    /**
     * @return ProcessorStore
     */
    private function getProcessorStore()
    {
        return $this->processorStore;
    }

    /**
     * PreProcess the message.
     *
     * @param  MessageInterface $message
     * @return MessageInterface
     */
    public function preProcessMessage(MessageInterface $message)
    {
        foreach ($this->getProcessorStore()->getProcessors() as $processor) {
            $message = $processor->preProcessMessage($message);
        }
        return $message;
    }

    /**
     * @param  MessageInterface $message
     * @param  RecipientInterface $recipient
     * @param  ChannelInterface $channel
     * @return MessageInterface
     */
    public function processMessage(MessageInterface $message, RecipientInterface $recipient, ChannelInterface $channel)
    {
        foreach ($this->getProcessorStore()->getProcessors() as $processor) {
            if ($processor->isHandling($channel)) {
                $message = $processor->processMessage($message, $recipient);
            }
        }
        return $message;
    }
}