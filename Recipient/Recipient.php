<?php

namespace Iweigel\NotifierBundle\Recipient;

use Iweigel\NotifierBundle\ParameterBag\ParameterBagTrait;

class Recipient implements RecipientInterface
{
    use ParameterBagTrait;

}