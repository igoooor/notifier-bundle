<?php

namespace Iweigel\NotifierBundle\Recipient;

use Iweigel\NotifierBundle\ParameterBag\ParameterBagInterface;

interface RecipientInterface
{
    /**
     * Get all the attached ParameterBags.
     *
     * @return ParameterBagInterface[]
     */
    public function getParameterBags();

    /**
     * Get a specific ParameterBag based on identifier.
     *
     * @param string $identifier
     * @return ParameterBagInterface
     */
    public function getParameterBag($identifier);

    /**
     * Check if this recipient has a specific ParameterBag.
     *
     * @param string $identifier
     * @return bool
     */
    public function hasParameterBag($identifier);

    /**
     * Add a ParameterBag.
     *
     * @param ParameterBagInterface $bag
     */
    public function addParameterBag(ParameterBagInterface $bag);

}