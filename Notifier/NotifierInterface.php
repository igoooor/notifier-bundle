<?php

namespace Iweigel\NotifierBundle\Notifier;

use Iweigel\NotifierBundle\Channel\ChannelInterface;
use Iweigel\NotifierBundle\Channel\ChannelStore;
use Iweigel\NotifierBundle\Message\MessageInterface;
use Iweigel\NotifierBundle\Processor\ProcessorInterface;
use Iweigel\NotifierBundle\Processor\ProcessorStore;
use Iweigel\NotifierBundle\Recipient\RecipientInterface;
use Iweigel\NotifierBundle\Response\NotifierResponse;

interface NotifierInterface
{
    /**
     * Inject your own processor store.
     *
     * @param ProcessorStore $processorStore
     */
    function setProcessorStore($processorStore);

    /**
     * Inject your own channel store.
     *
     * @param ChannelStore $channelStore
     */
    function setChannelStore($channelStore);

    /**
     * @return ChannelStore
     */
    function getChannelStore();

    /**
     * Add a channel.
     *
     * @param ChannelInterface $channel
     */
    function addChannel(ChannelInterface $channel);

    /**
     * @return ProcessorStore
     */
    function getProcessorStore();

    /**
     * Add a processor.
     *
     * @param ProcessorInterface $processor
     */
    function addProcessor(ProcessorInterface $processor);

    /**
     * Send a message to any number of recipients.
     *
     * @param MessageInterface $message
     * @param RecipientInterface[] $recipients
     * @return NotifierResponse
     */
    function sendMessage(MessageInterface $message, array $recipients);

    /**
     * Apply all logic to get the correct channels for the current recipient.
     *
     * @param  MessageInterface   $message
     * @param  RecipientInterface $recipient
     * @return ChannelInterface[]
     */
    function getChannels(MessageInterface $message, RecipientInterface $recipient);
}