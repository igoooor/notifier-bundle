<?php

namespace Iweigel\NotifierBundle\Notifier;

use Iweigel\NotifierBundle\Channel\ChannelInterface;
use Iweigel\NotifierBundle\Channel\ChannelResolverInterface;
use Iweigel\NotifierBundle\Channel\ChannelStore;
use Iweigel\NotifierBundle\Event\PostProcessorsCollectionEvent;
use Iweigel\NotifierBundle\Event\PostSendEvent;
use Iweigel\NotifierBundle\Exception\UnexpectedResponseException;
use Iweigel\NotifierBundle\Message\MessageInterface;
use Iweigel\NotifierBundle\Message\MessageProcessor;
use Iweigel\NotifierBundle\Processor\ProcessorInterface;
use Iweigel\NotifierBundle\Processor\ProcessorStore;
use Iweigel\NotifierBundle\Recipient\RecipientInterface;
use Iweigel\NotifierBundle\Response\ChannelResponse;
use Iweigel\NotifierBundle\Response\NotifierResponse;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class Notifier implements NotifierInterface
{
    /**
     * @var ChannelStore
     */
    private $channelStore;

    /**
     * @var ProcessorStore
     */
    private $processorStore;

    /**
     * @var ChannelResolverInterface
     */
    private $channelResolver;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * Construct Notifier with your own logic.
     *
     * @param ChannelResolverInterface $channelResolver
     */
    public function __construct(ChannelResolverInterface $channelResolver)
    {
        $this->channelResolver = $channelResolver;
    }

    /**
     * Inject your own processor store.
     *
     * @param ProcessorStore $processorStore
     */
    public function setProcessorStore($processorStore)
    {
        $this->processorStore = $processorStore;
    }

    /**
     * Inject your own channel store.
     *
     * @param ChannelStore $channelStore
     */
    public function setChannelStore($channelStore)
    {
        $this->channelStore = $channelStore;
    }

    /**
     * @return ChannelStore
     */
    public function getChannelStore()
    {
        if (is_null($this->channelStore)) {
            $this->channelStore = new ChannelStore();
        }

        return $this->channelStore;
    }

    /**
     * Add a channel.
     *
     * @param ChannelInterface $channel
     */
    public function addChannel(ChannelInterface $channel)
    {
        $this
            ->getChannelStore()
            ->addChannel($channel);
        $processor = $channel->getProcessor();
        if ($processor) {
            $this->getProcessorStore()->addProcessor($processor);
        }
    }

    /**
     * @return ProcessorStore
     */
    public function getProcessorStore()
    {
        if (is_null($this->processorStore)) {
            $this->processorStore = new ProcessorStore();
        }
        return $this->processorStore;
    }

    /**
     * Add a processor.
     *
     * @param ProcessorInterface $processor
     */
    public function addProcessor(ProcessorInterface $processor)
    {
        $this
            ->getProcessorStore()
            ->addProcessor($processor);
    }

    /**
     * Send a message to any number of recipients.
     *
     * @param MessageInterface $message
     * @param RecipientInterface[] $recipients
     * @return NotifierResponse
     * @throws UnexpectedResponseException
     */
    public function sendMessage(MessageInterface $message, array $recipients)
    {
        $processorStore = $this->getMessageProcessorStore($message);

        if ($this->hasListeners(NotifierEvents::POST_PROCESSORS_COLLECTION)) {
            $event = new PostProcessorsCollectionEvent($this, $processorStore);
            $this->eventDispatcher->dispatch(NotifierEvents::POST_PROCESSORS_COLLECTION, $event);
        }

        $messageProcessor = new MessageProcessor($processorStore);
        $message = $messageProcessor->preProcessMessage($message);
        $responses = [];
        foreach ($recipients as $recipient) {
            foreach ($this->getChannels($message, $recipient) as $channel) {
                $processedMessage = $messageProcessor
                    ->processMessage(clone($message), $recipient, $channel);
                if ($channel->isHandling($processedMessage, $recipient)) {
                    $response = $channel->send($processedMessage, $recipient);

                    if (!$response instanceof ChannelResponse) {
                        throw new UnexpectedResponseException();
                    }
                    $responses[$channel->getIdentifier()] = $response;
                }
            }
        }

        $notifierResponse = new NotifierResponse($responses);

        if ($this->hasListeners(NotifierEvents::POST_SEND)) {
            $event = new PostSendEvent($this, $notifierResponse);
            $this->eventDispatcher->dispatch(NotifierEvents::POST_SEND, $event);
        }

        return $notifierResponse;
    }

    /**
     * Return processors of each channel of given message with user processors
     *
     * @param MessageInterface $message
     * @return ProcessorStore
     */
    protected function getMessageProcessorStore(MessageInterface $message)
    {
        $processors = array_merge($this->getProcessorStore()->getProcessors(), $this->getMessageChannelsProcessors($message));
        $processors = array_map('unserialize', array_unique(array_map('serialize', $processors)));
        $processorStore = new ProcessorStore($processors);

        return $processorStore;
    }

    /**
     * Return processors of each channel of given message
     *
     * @param MessageInterface $message
     * @return ProcessorInterface[]
     */
    protected function getMessageChannelsProcessors(MessageInterface $message)
    {
        $channels = $this->getAllChannels($message);

        /** @var ProcessorInterface[] $processors */
        $processors = [];

        foreach ($channels as $channel) {
            $processor = $channel->getProcessor();
            if ($processor !== null) {
                $processors[] = $processor;
            }
        }

        return $processors;
    }

    /**
     * @param MessageInterface $message
     * @return ChannelInterface[]
     */
    protected function getAllChannels(MessageInterface $message)
    {
        return $this
            ->channelResolver
            ->getChannels($message->getType(), $this->getChannelStore());
    }

    /**
     * Apply all logic to get the correct channels for the current recipient.
     *
     * @param  MessageInterface   $message
     * @param  RecipientInterface $recipient
     * @return ChannelInterface[]
     */
    public function getChannels(MessageInterface $message, RecipientInterface $recipient)
    {
        $channels = $this->getAllChannels($message);

        return $this
            ->channelResolver
            ->filterChannels($recipient, $message->getType(), $channels);
    }

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param string $eventName
     * @return boolean
     */
    protected function hasListeners($eventName)
    {
        return $this->eventDispatcher !== null && $this->eventDispatcher->hasListeners($eventName);
    }
}