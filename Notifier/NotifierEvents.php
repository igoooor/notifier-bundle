<?php

namespace Iweigel\NotifierBundle\Notifier;

class NotifierEvents {

    const POST_SEND = 'notifier.post_send';

    const POST_PROCESSORS_COLLECTION = 'notifier.post_processors_collection';

}
