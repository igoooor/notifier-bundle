<?php

namespace Iweigel\NotifierBundle\Exception;

/**
 * Thrown when a value does not match an expected type.
 *
 * @author Bernhard Schussek <bschussek@gmail.com>
 */
class UnexpectedResponseException extends \Exception
{

}
