<?php

namespace Iweigel\NotifierBundle\Channel;

use Iweigel\NotifierBundle\Recipient\RecipientInterface;
use Iweigel\NotifierBundle\Type\Type;
use Iweigel\NotifierBundle\Type\TypeInterface;

class ChannelResolver implements ChannelResolverInterface
{
    /**
     * @var array
     */
    private $rules;

    /**
     * @var RecipientChannelResolverInterface
     */
    private $recipientChannelResolver;

    public function __construct(RecipientChannelResolverInterface $recipientChannelResolver)
    {
        $this->recipientChannelResolver = $recipientChannelResolver;
    }

    /**
     * @param $type
     * @param $channel
     */
    public function addRule($type, ChannelInterface $channel)
    {
        $this->rules[$type][] = $channel;
    }

    /**
     * @param  TypeInterface      $type
     * @param  ChannelStore       $channelStore
     * @return ChannelInterface[]
     */
    public function getChannels(TypeInterface $type, ChannelStore $channelStore)
    {
        if ($type instanceof Type && isset($this->rules[$type->getName()])) {
            return array_merge($this->rules[$type->getName()], $channelStore->getChannels());
        }
        return $channelStore->getChannels();
    }

    /**
     * @param  RecipientInterface $recipient
     * @param  TypeInterface      $type
     * @param  ChannelInterface[] $channels
     * @return ChannelInterface[]
     */
    public function filterChannels(RecipientInterface $recipient, TypeInterface $type, array $channels)
    {
        return $this->recipientChannelResolver->filterChannels($recipient, $type, $channels);
    }
}