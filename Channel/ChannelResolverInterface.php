<?php

namespace Iweigel\NotifierBundle\Channel;

use Iweigel\NotifierBundle\Recipient\RecipientInterface;
use Iweigel\NotifierBundle\Type\TypeInterface;

interface ChannelResolverInterface
{

    /**
     * @param  RecipientInterface $recipient
     * @param  TypeInterface      $type
     * @param  ChannelInterface[] $channels
     * @return ChannelInterface[]
     */
    public function filterChannels(RecipientInterface $recipient, TypeInterface $type, array $channels);

    /**
     * Get all channels for a given type of message.
     *
     * @param  TypeInterface      $type
     * @param  ChannelStore       $channelStore
     * @return ChannelInterface[]
     */
    public function getChannels(TypeInterface $type, ChannelStore $channelStore);
}