<?php

namespace Iweigel\NotifierBundle\Channel;

class ChannelStore
{
    /**
     * @var ChannelInterface[]
     */
    private $channels;

    /**
     * @param array $channels
     */

    public function __construct($channels = array())
    {
        $this->channels = $channels;
    }

    /**
     * @param ChannelInterface $channel
     */
    public function addChannel(ChannelInterface $channel)
    {
        $this->channels[] = $channel;
    }

    /**
     * @return ChannelInterface[]
     */
    public function getChannels()
    {
        return $this->channels;
    }
}