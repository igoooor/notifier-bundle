<?php

namespace Iweigel\NotifierBundle\Channel;

use Iweigel\NotifierBundle\Channel\ChannelInterface;
use Iweigel\NotifierBundle\Recipient\RecipientInterface;
use Iweigel\NotifierBundle\Type\TypeInterface;

interface RecipientChannelResolverInterface
{
    /**
     * @param  RecipientInterface $recipient
     * @param  TypeInterface      $type
     * @param  ChannelInterface[] $channels
     * @return ChannelInterface[]
     */
    public function filterChannels(RecipientInterface $recipient, TypeInterface $type, array $channels);
}