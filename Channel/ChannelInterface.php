<?php

namespace Iweigel\NotifierBundle\Channel;

use Iweigel\NotifierBundle\Message\MessageInterface;
use Iweigel\NotifierBundle\Response\ChannelResponse;
use Iweigel\NotifierBundle\Recipient\RecipientInterface;
use Iweigel\NotifierBundle\Processor\ProcessorInterface;

interface ChannelInterface
{
    /**
     * @return string
     */
    public function getIdentifier();

    /**
     * Test if the channel can send the message given the supplied parameters.
     *
     * @param  MessageInterface   $message
     * @param  RecipientInterface $recipient
     * @return bool
     */
    public function isHandling(MessageInterface $message, RecipientInterface $recipient);

    /**
     * Send the message.
     *
     * @param  MessageInterface   $message
     * @param  RecipientInterface $recipient
     * @return ChannelResponse
     */
    public function send(MessageInterface $message, RecipientInterface $recipient);

    /**
     * Get processors required by this channel.
     *
     * @return ProcessorInterface|null
     */
    public function getProcessor();

}