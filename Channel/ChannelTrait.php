<?php

namespace Iweigel\NotifierBundle\Channel;

use Iweigel\NotifierBundle\Message\MessageInterface;
use Iweigel\NotifierBundle\ParameterBag\ParameterBagInterface;
use Iweigel\NotifierBundle\Recipient\RecipientInterface;

trait ChannelTrait
{

    public abstract function getIdentifier();

    /**
     * Test if the channel can send the message given the supplied parameters.
     *
     * @param  MessageInterface   $message
     * @param  RecipientInterface $recipient
     *
     * @return bool
     */
    public function isHandling(MessageInterface $message, RecipientInterface $recipient)
    {
        return $message->hasParameterBag($this->getIdentifier()) && $recipient->hasParameterBag($this->getIdentifier());
    }

    /**
     * Get the MailParameterBag from the recipient.
     *
     * @param RecipientInterface $recipient
     *
     * @return ParameterBagInterface
     */
    protected function getRecipientBag(RecipientInterface $recipient)
    {
        return $recipient->getParameterBag($this->getIdentifier());
    }

    /**
     * Get the MailParameterBag from the message.
     *
     * @param MessageInterface $message
     *
     * @return ParameterBagInterface
     */
    protected function getMessageBag(MessageInterface $message)
    {
        return $message->getParameterBag($this->getIdentifier());
    }

}