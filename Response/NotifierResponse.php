<?php

namespace Iweigel\NotifierBundle\Response;

class NotifierResponse
{
    /**
     * @var ChannelResponse[]
     */
    private $responses;

    /**
     * @param ChannelResponse[] $responses
     */
    public function __construct($responses = [])
    {
        $this->responses = $responses;
    }

    /**
     * Get all responses.
     *
     * @return ChannelResponse[]
     */
    public function getResponses()
    {
        return $this->responses;
    }

    /**
     * Get a specific ChannelResponse based on identifier.
     *
     * @param string $identifier
     * @return ChannelResponse
     */
    public function getResponse($identifier)
    {
        if ($this->hasResponse($identifier)) {
            return $this->responses[$identifier];
        }

        return null;
    }

    /**
     * Check if this message has a specific ChannelResponse.
     *
     * @param string $identifier
     * @return bool
     */
    public function hasResponse($identifier)
    {
        return isset($this->responses[$identifier]);
    }
}