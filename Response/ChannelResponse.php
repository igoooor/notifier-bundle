<?php

namespace Iweigel\NotifierBundle\Response;

class ChannelResponse
{
    /**
     * @var boolean
     */
    protected $success;

    /**
     * @var mixed
     */
    protected $responseDetail;

    /**
     * NotifierResponse constructor.
     * @param boolean $success
     * @param mixed $responseDetail
     */
    public function __construct($success = true, $responseDetail = null)
    {
        $this->success = $success;
        $this->responseDetail = $responseDetail;
    }

    /**
     * @return boolean
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @return mixed
     */
    public function getResponseDetail()
    {
        return $this->responseDetail;
    }
}