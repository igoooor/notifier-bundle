<?php

namespace Iweigel\NotifierBundle\Event;


use Iweigel\NotifierBundle\Notifier\NotifierInterface;
use Iweigel\NotifierBundle\Processor\ProcessorStore;

class PostProcessorsCollectionEvent extends NotifierEvent {

    /**
     * @var ProcessorStore
     */
    protected $processorStore;

    /**
     * PostSendEvent constructor.
     * @param NotifierInterface $notifier
     * @param ProcessorStore $processorStore
     */
    public function __construct(NotifierInterface $notifier, ProcessorStore $processorStore)
    {
        parent::__construct($notifier);

        $this->processorStore = $processorStore;
    }

    /**
     * @return ProcessorStore
     */
    public function getProcessorStore() {
        return $this->processorStore;
    }
}
