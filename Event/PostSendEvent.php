<?php

namespace Iweigel\NotifierBundle\Event;


use Iweigel\NotifierBundle\Notifier\NotifierInterface;
use Iweigel\NotifierBundle\Response\NotifierResponse;

class PostSendEvent extends NotifierEvent {

    /**
     * @var NotifierResponse
     */
    protected $notifierResponse;

    /**
     * PostSendEvent constructor.
     * @param NotifierInterface $notifier
     * @param NotifierResponse $notifierResponse
     */
    public function __construct(NotifierInterface $notifier, NotifierResponse $notifierResponse)
    {
        parent::__construct($notifier);

        $this->notifierResponse = $notifierResponse;
    }

    /**
     * @return NotifierResponse
     */
    public function getNotifierResponse() {
        return $this->notifierResponse;
    }
}
