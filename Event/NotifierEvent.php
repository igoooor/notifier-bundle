<?php

namespace Iweigel\NotifierBundle\Event;

use Iweigel\NotifierBundle\Notifier\NotifierInterface;
use Symfony\Component\EventDispatcher\Event;

abstract class NotifierEvent extends Event {

    /**
     * @var NotifierInterface
     */
    protected $notifier;

    /**
     * @param NotifierInterface $notifier
     */
    public function __construct(NotifierInterface $notifier) {
        $this->notifier = $notifier;
    }

    /**
     * @return NotifierInterface
     */
    public function getNotifier() {
        return $this->notifier;
    }

}
