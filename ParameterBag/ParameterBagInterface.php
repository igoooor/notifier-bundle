<?php

namespace Iweigel\NotifierBundle\ParameterBag;

interface ParameterBagInterface
{
    public function getIdentifier();
}