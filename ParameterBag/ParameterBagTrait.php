<?php

namespace Iweigel\NotifierBundle\ParameterBag;

trait ParameterBagTrait
{
    /**
     * @var ParameterBagInterface[]
     */
    private $parameterBags = array();

    /**
     * Get all the attached ParameterBags.
     *
     * @return ParameterBagInterface[]
     */
    public function getParameterBags()
    {
        return $this->parameterBags;
    }

    /**
     * Get a specific ParameterBag based on identifier.
     *
     * @param string $identifier
     * @return ParameterBagInterface
     */
    public function getParameterBag($identifier)
    {
        if ($this->hasParameterBag($identifier)) {
            return $this->parameterBags[$identifier];
        }

        return null;
    }

    /**
     * Check if this message has a specific ParameterBag.
     *
     * @param string $identifier
     * @return bool
     */
    public function hasParameterBag($identifier)
    {
        return isset($this->parameterBags[$identifier]);
    }

    /**
     * Add a ParameterBag.
     *
     * @param ParameterBagInterface $bag
     */
    public function addParameterBag(ParameterBagInterface $bag)
    {
        $this->parameterBags[$bag->getIdentifier()] = $bag;
    }
}