<?php

namespace Iweigel\NotifierBundle\Processor;


class ProcessorStore
{
    /**
     * @var ProcessorInterface[]
     */
    private $processors;

    /**
     * @param ProcessorInterface[] $processors
     */
    public function __construct($processors = [])
    {
        $this->processors = $processors;
    }

    /**
     * Add a processor.
     *
     * @api
     *
     * @param ProcessorInterface $processor
     */
    public function addProcessor(ProcessorInterface $processor)
    {
        $this->processors[] = $processor;
    }

    /**
     * Get all processors.
     *
     * @api
     *
     * @return ProcessorInterface[]
     */
    public function getProcessors()
    {
        return $this->processors;
    }
}