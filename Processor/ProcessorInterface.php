<?php

namespace Iweigel\NotifierBundle\Processor;

use Iweigel\NotifierBundle\Channel\ChannelInterface;
use Iweigel\NotifierBundle\Message\MessageInterface;
use Iweigel\NotifierBundle\Recipient\RecipientInterface;

interface ProcessorInterface
{
    public function getIdentifier();

    /**
     * Test if the processor can process the message given the supplied parameters.
     *
     * @param  ChannelInterface $channel
     * @return bool
     */
    public function isHandling(ChannelInterface $channel);

    /**
     * @param  MessageInterface $message
     * @return MessageInterface
     */
    public function preProcessMessage(MessageInterface $message);

    /**
     * @param  MessageInterface   $message
     * @param  RecipientInterface $recipient
     * @return MessageInterface
     */
    public function processMessage(MessageInterface $message, RecipientInterface $recipient);
}