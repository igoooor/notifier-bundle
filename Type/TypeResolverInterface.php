<?php

namespace Iweigel\NotifierBundle\Type;
use Iweigel\NotifierBundle\Channel\ChannelInterface;
use Iweigel\NotifierBundle\Channel\ChannelStore;

interface TypeResolverInterface
{
    /**
     * Get all channels for a given type of message.
     *
     * @param  TypeInterface      $type
     * @param  ChannelStore       $channelStore
     * @return ChannelInterface[]
     */
    public function getChannels(TypeInterface $type, ChannelStore $channelStore);
}